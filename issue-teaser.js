(function($){
    $(document).ready(function(){
        var link = $('.node-issue .field-name-field-link a').attr('href');
        $('article.node-issue h2 a').attr('href', link);
        $('.node-issue .field-name-field-link a').hide();
        $('article.node-issue .field-name-field-image').append($('article.node-issue .field-name-body').get()[0]);
    });
})(jQuery)
