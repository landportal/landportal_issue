<?php
// Plugin definition
$plugin = array(
  'title' => t('Brenham Issue'),
  'icon' => 'radix-brenham-issue.png',
  'category' => t('Landportal'),
  'theme' => 'radix_brenham_issue',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
