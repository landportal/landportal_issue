<?php
// Plugin definition
$plugin = array(
  'title' => t('Brenham Flipped Issue'),
  'icon' => 'radix-brenham-flipped-issue.png',
  'category' => t('Landportal'),
  'theme' => 'radix_brenham_flipped_issue',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
