<?php
// Plugin definition
$plugin = array(
  'title' => t('Marcello Issue'),
  'icon' => 'radix-marcello-issue.png',
  'category' => t('Landportal'),
  'theme' => 'radix_marcello_issue',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
