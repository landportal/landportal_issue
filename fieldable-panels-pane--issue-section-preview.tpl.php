<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_suffix); ?>
  <div class="container-fluid">
    <div class="row">
    <?php
    preg_match_all('/(.*)"(.*)"(.*)/', $content['title']['#value'], $matches, PREG_PATTERN_ORDER);
    $link = $matches[2];
    ?>
      <div class="col-md-3 radix-layouts-sidebar panel-panel">
        <div class="panel-panel-inner">
          <?php //print render($content['field_image']); ?>
          <?php print l(theme('image_style', array('style_name' => '150x100', 'path' => $content['field_image']['#items'][0]['uri'], 'width' =>  $content['field_image']['#items'][0]['width'], 'height'=> $content['field_image']['#items'][0]['height'] )), $link[0] ,array('html' => TRUE, 'attributes' => array('class' => 'image-issue-section-preview'))); ?>
        </div>
      </div>
      <div class="col-md-9 radix-layouts-content panel-panel">
        <div class="panel-panel-inner">


          <?php 
             $content['title']['#access'] = TRUE;
             print render($content['title']); 
          ?>
         
          <?php print render($content['field_description']); ?>
        </div>
      </div>
    </div>
  </div>

</div>


