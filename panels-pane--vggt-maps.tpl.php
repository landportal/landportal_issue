<?php
/**
 * @file Panels-pane.tpl.php
 * Main panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if ($pane_prefix): ?>
  <?php print $pane_prefix; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <<?php print $title_heading; ?><?php print $title_attributes; ?>>
      <?php print $title; ?>
    </<?php print $title_heading; ?>>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($feeds): ?>
    <div class="feed">
      <?php print $feeds; ?>
    </div>
  <?php endif; ?>

  <div class="pane-content">
    <?php // print render($content); ?>


<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/linkify.min.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/highcharts.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/lbvis.js?v=0.2.2"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/lbvis.data.js?v=0.2.2"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/plugins/map.js?v=0.2.0"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/map_data.js?v=0.2.0"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/linkify-jquery.min.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/modules/stock.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/modules/map.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/modules/exporting.js?v=0.0.1"></script>
<script src="https://landportal.org/sites/all/libraries/js-view-coda/js/vendor/highcharts-more.js?v=0.0.1"></script>

    <script>var $ = jQuery.noConflict(); $(document).ready(function($) { LBV = new lbvis({prefix: '/sparql?query='}); });</script>
<script>jQuery(document).ready(function () { LBV.ready().done(function () { worldMap = new lbvisMap(map_data, LBV, {"series":[{"name":"click to visit","data":[{"id":"DZA","value":0},{"id":"AGO","value":1},{"id":"BEN","value":0},{"id":"BWA","value":1},{"id":"IOT","value":0},{"id":"BFA","value":1},{"id":"BDI","value":1},{"id":"CMR","value":1},{"id":"CPV","value":0},{"id":"CAF","value":1},{"id":"TCD","value":1},{"id":"COM","value":0},{"id":"COG","value":0},{"id":"COD","value":1},{"id":"DJI","value":0},{"id":"EGY","value":1},{"id":"GNQ","value":0},{"id":"ERI","value":0},{"id":"ETH","value":1},{"id":"ATF","value":0},{"id":"GAB","value":0},{"id":"GMB","value":0},{"id":"GHA","value":1},{"id":"GIN","value":1},{"id":"GNB","value":0},{"id":"CIV","value":1},{"id":"KEN","value":2},{"id":"LSO","value":0},{"id":"LBR","value":1},{"id":"LBY","value":1},{"id":"MDG","value":1},{"id":"MWI","value":1},{"id":"MLI","value":1},{"id":"MRT","value":0},{"id":"MUS","value":0},{"id":"MYT","value":0},{"id":"MAR","value":1},{"id":"MOZ","value":2},{"id":"NAM","value":1},{"id":"NER","value":1},{"id":"NGA","value":1},{"id":"REU","value":0},{"id":"RWA","value":2},{"id":"SHN","value":0},{"id":"STP","value":0},{"id":"SEN","value":1},{"id":"SYC","value":0},{"id":"SLE","value":1},{"id":"SOM","value":0},{"id":"ZAF","value":2},{"id":"SSD","value":1},{"id":"SDN","value":1},{"id":"SWZ","value":0},{"id":"TZA","value":2},{"id":"TGO","value":0},{"id":"TUN","value":0},{"id":"UGA","value":2},{"id":"ESH","value":0},{"id":"ZMB","value":2},{"id":"ZWE","value":1},{"id":"AIA","value":0},{"id":"ATG","value":0},{"id":"ARG","value":0},{"id":"ABW","value":0},{"id":"BHS","value":0},{"id":"BRB","value":0},{"id":"BLZ","value":0},{"id":"BMU","value":0},{"id":"BOL","value":2},{"id":"BES","value":0},{"id":"BVT","value":0},{"id":"BRA","value":2},{"id":"VGB","value":0},{"id":"CAN","value":0},{"id":"CYM","value":0},{"id":"CHL","value":0},{"id":"COL","value":2},{"id":"CRI","value":0},{"id":"CUB","value":0},{"id":"CUW","value":0},{"id":"DMA","value":0},{"id":"DOM","value":1},{"id":"ECU","value":2},{"id":"SLV","value":1},{"id":"FLK","value":0},{"id":"GUF","value":0},{"id":"GRL","value":0},{"id":"GRD","value":0},{"id":"GLP","value":0},{"id":"GTM","value":1},{"id":"GUY","value":0},{"id":"HTI","value":1},{"id":"HND","value":1},{"id":"JAM","value":1},{"id":"MTQ","value":0},{"id":"MEX","value":1},{"id":"MSR","value":0},{"id":"NIC","value":1},{"id":"PAN","value":0},{"id":"PRY","value":2},{"id":"PER","value":2},{"id":"PRI","value":0},{"id":"BLM","value":0},{"id":"KNA","value":0},{"id":"LCA","value":0},{"id":"SPM","value":0},{"id":"VCT","value":0},{"id":"MAF","value":0},{"id":"SXM","value":0},{"id":"SGS","value":0},{"id":"SUR","value":0},{"id":"TTO","value":0},{"id":"TCA","value":0},{"id":"USA","value":0},{"id":"VIR","value":0},{"id":"URY","value":0},{"id":"VEN","value":0},{"id":"AFG","value":1},{"id":"ARM","value":0},{"id":"AZE","value":0},{"id":"BHR","value":0},{"id":"BGD","value":2},{"id":"BTN","value":0},{"id":"BRN","value":0},{"id":"KHM","value":2},{"id":"CHN","value":0},{"id":"CYP","value":0},{"id":"PRK","value":0},{"id":"GEO","value":1},{"id":"HKG","value":0},{"id":"IND","value":2},{"id":"IDN","value":1},{"id":"IRN","value":0},{"id":"IRQ","value":0},{"id":"ISR","value":0},{"id":"JPN","value":0},{"id":"JOR","value":0},{"id":"KAZ","value":0},{"id":"KWT","value":0},{"id":"KGZ","value":1},{"id":"LAO","value":2},{"id":"LBN","value":0},{"id":"MAC","value":0},{"id":"MYS","value":0},{"id":"MDV","value":0},{"id":"MNG","value":1},{"id":"MMR","value":2},{"id":"NPL","value":1},{"id":"OMN","value":0},{"id":"PAK","value":1},{"id":"PSE","value":0},{"id":"PHL","value":1},{"id":"QAT","value":0},{"id":"KOR","value":0},{"id":"SAU","value":0},{"id":"SGP","value":0},{"id":"LKA","value":0},{"id":"SYR","value":0},{"id":"TJK","value":1},{"id":"THA","value":2},{"id":"TLS","value":1},{"id":"TUR","value":0},{"id":"TKM","value":0},{"id":"ARE","value":0},{"id":"UZB","value":0},{"id":"VNM","value":2},{"id":"YEM","value":1},{"id":"ALA","value":0},{"id":"ALB","value":1},{"id":"AND","value":0},{"id":"AUT","value":0},{"id":"BLR","value":0},{"id":"BEL","value":0},{"id":"BIH","value":0},{"id":"BGR","value":0},{"id":"HRV","value":0},{"id":"CZE","value":0},{"id":"DNK","value":0},{"id":"EST","value":0},{"id":"FRO","value":0},{"id":"FIN","value":0},{"id":"FRA","value":0},{"id":"DEU","value":0},{"id":"GIB","value":0},{"id":"GRC","value":0},{"id":"GGY","value":0},{"id":"VAT","value":0},{"id":"HUN","value":0},{"id":"ISL","value":0},{"id":"IRL","value":0},{"id":"IMN","value":0},{"id":"ITA","value":0},{"id":"JEY","value":0},{"id":"LVA","value":0},{"id":"LIE","value":0},{"id":"LTU","value":0},{"id":"LUX","value":0},{"id":"MKD","value":0},{"id":"MLT","value":0},{"id":"MDA","value":0},{"id":"MCO","value":0},{"id":"MNE","value":0},{"id":"NLD","value":0},{"id":"NOR","value":0},{"id":"POL","value":0},{"id":"PRT","value":0},{"id":"ROU","value":0},{"id":"RUS","value":0},{"id":"SMR","value":0},{"id":"SRB","value":0},{"id":"SVK","value":0},{"id":"SVN","value":0},{"id":"ESP","value":0},{"id":"SJM","value":0},{"id":"SWE","value":0},{"id":"CHE","value":0},{"id":"UKR","value":0},{"id":"GBR","value":0},{"id":"ASM","value":0},{"id":"ATA","value":0},{"id":"AUS","value":0},{"id":"FJI","value":0},{"id":"GUM","value":0},{"id":"CXR","value":0},{"id":"COK","value":0},{"id":"KIR","value":0},{"id":"NCL","value":0},{"id":"CCK","value":0},{"id":"PYF","value":0},{"id":"MHL","value":0},{"id":"PNG","value":0},{"id":"HMD","value":0},{"id":"FSM","value":0},{"id":"NIU","value":0},{"id":"SLB","value":0},{"id":"NRU","value":0},{"id":"NZL","value":0},{"id":"PCN","value":0},{"id":"VUT","value":0},{"id":"XKX","value":0},{"id":"NFK","value":0},{"id":"MNP","value":0},{"id":"WSM","value":0},{"id":"PLW","value":0},{"id":"TKL","value":0},{"id":"TWN","value":0},{"id":"TON","value":0},{"id":"UMI","value":0},{"id":"TUV","value":0},{"id":"WLF","value":0}]}],"colors":{"background":"#FFFFFF","hover":"#F5A623","select":"#F5A623","borders":"#FFFFFF","min":"#BBD6D8","max":"#E58517","na":"#BBD6D8"},"map":{"selectable":true,"legend":true,"nav":true,"tooltip":function () { return this.name + (this.value ?  ' portfolio' : ' overview page'); },"events":{ click: function () { window.location.href = '\/book\/countries\/' + this.id; } }},"indicators":[],"cache":[],"target":"#block-landbook-view-coda-lbvc-map-wrapper"}); worldMap.init(); }) });</script>

<div id="block-landbook-view-coda-lbvc-map-wrapper"> </div>


  </div>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more-link">
      <?php print $more; ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($pane_suffix): ?>
  <?php print $pane_suffix; ?>
<?php endif; ?>
